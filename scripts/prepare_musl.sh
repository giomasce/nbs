#!/bin/bash

. scripts/common.sh

mkdir -p prepare/musl
mkdir -p prepare/root
cd prepare/musl
../../root/src/musl/configure --prefix=/stage1 --syslibdir=/stage1/lib --host=$NBS_ARCH CROSS_COMPILE=$NBS_TRIPLET-
make
make install DESTDIR=../root
