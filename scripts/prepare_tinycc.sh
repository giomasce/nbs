#!/bin/bash

. scripts/common.sh

mkdir -p prepare/tinycc
mkdir -p prepare/root
cd prepare/tinycc
../../root/src/tinycc/configure --config-musl --prefix=/stage1 --sysincludepaths=/stage1/include --libpaths=/stage1/lib --crtprefix=/stage1/lib --cpu=$NBS_ARCH --triplet=$NBS_ARCH-linux-musl --cc=$NBS_TRIPLET-gcc --elfinterp=/stage1/lib/ld-musl-$NBS_ARCH.so.1
make
# Recompile tcc statically, otherwise it won't work in the chroot
$NBS_TRIPLET-gcc -static -o tcc tcc.o libtcc.a -lm -ldl
make install DESTDIR=../root
