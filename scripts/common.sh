#!/bin/bash

set -e

mkdir -p root

if [ -z "$NBS_ARCH" ] ; then
    echo "NBS_ARCH not set. Please, set it!"
    exit 1
fi

echo "Using NBS_ARCH = $NBS_ARCH"

if [ -z "$NBS_DEB_ARCH" ] ; then
    echo "NBS_DEB_ARCH not set. Please, set it!"
    exit 1
fi

echo "Using NBS_DEB_ARCH = $NBS_DEB_ARCH"

if [ -z "$NBS_TRIPLET" ] ; then
    echo "NBS_TRIPLET not set. Please, set it!"
    exit 1
fi

echo "Using NBS_TRIPLET = $NBS_TRIPLET"
