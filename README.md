
# NBS: NBS BootStrap

The software in this repository implements a bootstrap of a running
GNU/Linux system starting from a minimal set of binaries. The minimal
starting set is composed of the Linux kernel, the musl libc and the
tinycc compiler. They are made available to the system as compiled
binaries, together with many other programs in source code form. When
the system is started, all the other programs are incrementally built
starting from the musl and tinycc, with the target to eventually build
a full running system.

NBS is currently able to create a i386 or amd64 system.

## First boostrapping stages

In order to launch the boostrap project you need to first install a
suitable cross toolchain (on Debian you can install package
`crossbuild-essential-i386` for i386 and `crossbuild-essential-amd64`
for amd64). Then you have to set a few environment variables for the
bootstrapping scripts sourcing the approriate script in your shell:

    $ . scripts/set_i386.sh      # for i386
    $ . scripts/set_amd64.sh     # for amd64

Then you can compile tinycc and musl:

    $ ./scripts/prepare_tinycc.sh
    $ ./scripts/prepare_musl.sh

The resulting binaries will be installed in the directory
`prepare/root`. You can copy them in the directory `root` with the
command:

    $ ./scripts/copy_root.sh

The directory `root` already contains the source code for all the
following programs to compile, so at last you can launch a last script
that locks itself inside the chroot in the directory `root` and
continues the compilation there inside.

    $ sudo ./script/continue.sh

It is unfortunately necessary to use `sudo` here, both to execute
chroot and to do some privileged actions to setup everything in the
chroot. If you do not trust these scripts, run them under some form of
container or virtual machine.

At last you can enter the bootstrapped system and use it, at least for
what the system is currently able to do (very little, if you are used
to a full GNU installation):

    $ sudo chroot root /bin/sh

## What happens then?

The script `continue.sh` only calls chroot and then calls a script
inside `root` to continue. Since at this point we do not have a shell
yet in the chroot, this is not a shell script, but a C script
(`root/src/continue.c`), compiled and run on the fly by tinycc. This
script sets up a minimal filesystem and then compiles dash, so that a
shell is finally available. It then calls a shell script to continue
further on (`root/src/continue.sh`).

Now we have a shell, but basically no available commands (only the
shell builtins). So we proceed immediately to compile a few temporary
basic programs (like `rm`, `cp`, `mkdir` and others, plus some more
advanced commands frequently used during compilation, like `sed` and
`grep`). The code is taken from the Heirloom project (see below for a
few more words on this choice). The executables are put in `/bintmp`
in the chroot: later we will compile (more) properly the whole
Heirloom project and remove the temporary executables.

Then we continue compiling:

 * `make` from the [GNU project](https://www.gnu.org/). We do not have
   GNU autotools nor `make` itself yet, so we just use a custom shell
   script.

 * `lex` and `yacc` from the [Heirloom
   project](http://heirloom.sourceforge.net/). The Heirloom project is
   a collection of standard UNIX utilities derived from code by
   Caldera and Sun. Differently from the analogous utilities from the
   GNU project, they can be compiled with a simple `Makefile` (and do
   not require all the complicated autotools machinery), therefore
   they are ideal for a boostrapping project. They are of course not
   completely compatible with the GNU utilities, but they more than
   enough for this stage. `lex` and `yacc` in particular have the
   advantage that do not depend on themselves to compile, differently
   from the more powerful `flex` and `bison`. The Heirloom project is
   unfortunately not maintained anymore since 2007. However the
   interface it implements is nowadays quite stable, so the need for
   updating the code is rather limited. At the same time, one might
   argue that for a boostrapping project it is better to have code
   that changes as little as possible.

 * Most of the other utilities from the Heirloom project. Some of them
   are disabled because they depend on libraries that are not
   available yet and because they are not very useful at this
   stage. After this step the temporary executables in `/bintmp` are
   deleted.

 * `m4` from the GNU project. An implementation of `m4` is also
   provided by the Heirloom project, but it is not sufficiently
   powerful for later usage. An earlier version used FreeBSD's `m4` at
   this stage, but that proved to be insufficient for later stages.

 * `flex`, using `yacc` from the Heirloom project. Unfortunately
   `flex` also depends on itself for compiling its own lexer, so a two
   stage approach was devised: first `flex` 2.5.11 is compiled, with
   its scanner definition manually modified so that it can be
   processed by `lex` for the Heirloom project (the required
   modifications are mostly syntactical, plus a few workarounds to
   avoid some `flex` advanced features). Then `flex` 2.6.4 (the latest
   at the time of writing) is compiled using the first stage to
   process its unmodified scanner definition.

 * `bison` from the GNU project, using `flex` that we have already
   compiled and, for its first boostrapping stage, a custom
   hand-written parser for its own grammar. See [the dedicated
   project](https://gitlab.com/giomasce/bison-bootstrap) for the
   details.

## Other material

NBS was [presented at DebConf
2019](https://debconf19.debconf.org/talks/152-bootstrappable-debian-bof/).

For other topics on bootstrappability, see the [Bootstrappable
website](http://bootstrappable.org/), the [Bootstrapping
wiki](https://bootstrapping.miraheze.org/wiki/Main_Page) and the
[collection of talks and notes curated by
OriansJ](https://github.com/oriansj/talk-notes).

## License

Each of the program that is built by NBS is copyrighted by its authors
and distributed under its licensing terms. See the individual files or
directories to see what they are. Most of these programs are lightly
patched to adapt them to the specific NBS environment, with such
patches released under the same terms of the original files
themselves.

The gluing scripts and programs that oversee the bootstrapping and
compilation processes are written and copyrighted by Giovanni
Mascellani <gio@debian.org>. They are released under the two-clauses
BSD license:

    Copyright © 2019, Giovanni Mascellani <gio@debian.org>
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice, this
       list of conditions and the following disclaimer.
    2. Redistributions in binary form must reproduce the above copyright notice,
       this list of conditions and the following disclaimer in the documentation
       and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
    ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
    WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
    DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
    ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
    LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
    ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
    SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
