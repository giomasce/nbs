#!/stage1/bin/dash

set -e

logexec() {
    echo "$@"
    $@
}

color_echo() {
    echo "\033[93m$@\033[0m"
}

export PATH=/bin:/bintmp

BASIC_TOOLS="cat rm cp mkdir chmod touch basename"
color_echo "Compiling some basic tools from heirloom before continuing... ($BASIC_TOOLS) "
for x in $BASIC_TOOLS ; do
    (cd /src/heirloom/$x; logexec tcc -O -fomit-frame-pointer -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64L -I../libcommon $x.c ../libcommon/*.c -o /bintmp/$x)
done
cp /bintmp/cp /bintmp/ln
cp /bintmp/cp /bintmp/mv
color_echo "Basic tools compiled!"

color_echo "Compiling other basic tools from heirloom... (echo ed sed grep)"
(cd /src/heirloom/echo ; logexec tcc -O -fomit-frame-pointer -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64L -I../libcommon echo.c -Dfunc='echo' main.c version.c ../libcommon/*.c -o /bintmp/echo)
(cd /src/heirloom/ed; logexec tcc -O -fomit-frame-pointer -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64L -I../libcommon -DSHELL='"/bin/sh"' ed.c -o /bintmp/ed)
(cd /src/heirloom/grep ; echo '' > config.h; logexec tcc -O -fomit-frame-pointer -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64L -I../libcommon alloc.c grep.c grid.c ggrep.c svid3.c ../libcommon/*.c -o /bintmp/grep; rm config.h)
(cd /src/heirloom/sed ; logexec tcc -O -fomit-frame-pointer -D_GNU_SOURCE -D_FILE_OFFSET_BITS=64L -I../libcommon sed0.c sed1.c version.c -o /bintmp/sed)
color_echo "Other basic tools compiled!"

color_echo "Create other fake tools... (ar true strip)"
echo '#!/bin/sh\ntcc -ar $@' > /bintmp/ar
echo '#!/bin/sh\n' > /bintmp/true
echo '#!/bin/sh\n' > /bintmp/strip
chmod 755 /bintmp/ar /bintmp/true /bintmp/strip
color_echo "Fake tools created!"

color_echo "Compiling make..."
(cd /src/make; dash compile.sh)
cp /src/make/src/make /stage1/bin/make
rm -f /bin/make
ln -s /stage1/bin/make /bin/make
color_echo "Compiled make!"

color_echo "Compiling heirloom-devtools..."
make -C /src/heirloom-devtools
rm -f /bin/yacc /bin/lex
mkdir -p /stage1/lib/lex
cp /src/heirloom-devtools/yacc/yacc /stage1/bin/yacc
cp /src/heirloom-devtools/yacc/yaccpar /stage1/lib/yaccpar
cp /src/heirloom-devtools/yacc/liby.a /stage1/lib/liby.a
ln -s /stage1/bin/yacc /bin/yacc
cp /src/heirloom-devtools/lex/lex /stage1/bin/lex
cp /src/heirloom-devtools/lex/nceucform /stage1/lib/lex/nceucform
cp /src/heirloom-devtools/lex/ncform /stage1/lib/lex/ncform
cp /src/heirloom-devtools/lex/nrform /stage1/lib/lex/nrform
cp /src/heirloom-devtools/lex/libl.a /stage1/lib/libl.a
ln -s /stage1/bin/lex /bin/lex
color_echo "Compiled heirloom-devtools!"

color_echo "Compiling heirloom..."
make -C /src/heirloom
make -C /src/heirloom install
for x in /stage1/usr/ucb/* /stage1/usr/5bin/* /stage1/usr/5bin/posix2001/* /stage1/usr/5bin/posix/* /stage1/usr/5bin/s42/* /stage1/usr/ccs/bin/* ; do
    y="/bin/`basename "$x"`"
    rm -f "$y"
    ln -s "$x" "$y"
done
rm -f /bin/posix /bin/posix2001 /bin/s42
color_echo "Compiled heirloom!"

color_echo "Removing temporary tools"
rm -fr /bintmp
PATH=/bin

color_echo "Preparing gnulib..."
(cd /src/gnulib/lib && ./preprocess_headers.sh)
make -C /src/gnulib/lib
color_echo "Prepared gnulib!"

color_echo "Compiling GNU m4 (based on version 1.4.18)..."
make -C /src/m4/src
cp /src/m4/src/m4 /stage1/bin/m4
mkdir -p /usr/bin
rm -f /usr/bin/m4
ln -s /stage1/bin/m4 /usr/bin/m4
color_echo "Compiled GNU m4!"

color_echo "Compiling flex (first stage, based on version 2.5.11)..."
make -C /src/flex1
cp /src/flex1/flex /stage1/bin/flex1
rm -f /bin/flex1
ln -s /stage1/bin/flex1 /bin/flex1
color_echo "Compiled flex (first stage)!"

color_echo "Compiling flex (based on version 2.6.4)..."
make -C /src/flex/src
cp /src/flex/src/flex /stage1/bin/flex
rm -f /bin/flex
ln -s /stage1/bin/flex /bin/flex
color_echo "Compiled flex!"

color_echo "Compiling bison (based on version 3.4.1)..."
cp /src/bison/lib/textstyle.in.h /src/bison/lib/textstyle.h
rm -f /src/bison/src/parse-gram.c /src/bison/src/parse-gram.h /src/bison/src/*.o
cp /src/bison-bootstrap/parse-gram.c /src/bison-bootstrap/parse-gram.h /src/bison/src
make -C /src/bison
mkdir -p /stage1/share/bison
cp -r /src/bison/data/skeletons /src/bison/data/m4sugar /stage1/share/bison
rm -f /stage1/bin/bison
cp /src/bison/bison /stage1/bin/bison
rm -f /bin/bison
ln -s /stage1/bin/bison /bin/bison
color_echo "Recompiling bison..."
cp /src/bison/src/parse-gram.y /src/bison/src/parse-gram.y.orig
cp /src/bison-bootstrap/parse-gram.y /src/bison/src
rm -f /src/bison/src/parse-gram.c /src/bison/src/parse-gram.h /src/bison/src/*.o /src/bison/bison
make -C /src/bison
rm -f /stage1/bin/bison
cp /src/bison/bison /stage1/bin/bison
color_echo "Recompiling bison again..."
cp /src/bison/src/parse-gram.y.orig /src/bison/src/parse-gram.y
rm -f /src/bison/src/parse-gram.c /src/bison/src/parse-gram.h /src/bison/src/*.o /src/bison/bison
make -C /src/bison
rm -f /stage1/bin/bison
cp /src/bison/bison /stage1/bin/bison
color_echo "Compiled bison!"
