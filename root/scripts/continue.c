#!/stage1/bin/tcc -run

#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>

char *compile_dash[] = {"/stage1/bin/tcc", "-run", "/src/dash/compile.c", "/stage1/bin/tcc", NULL};
char *continue_sh[] = {"/scripts/continue.sh", NULL};

void color_echo(const char *line) {
    printf("\033[93m%s\033[0m\n", line);
}

int main() {
    color_echo("Creating basic filesystem...");
    unlink("/dev/null");
    mkdir("/dev", 0755);
    mkdir("/bin", 0755);
    mkdir("/bintmp", 0755);
    mkdir("/tmp", 0755);
    int res = mknod("/dev/null", S_IFCHR | 0666, makedev(1, 3));
    if (res) {
        fprintf(stderr, "Failed to create basic filesystem\n");
        exit(-1);
    }

    color_echo("Compiling dash...");
    // Compile dash
    pid_t pid = fork();
    if (pid == 0) {
        execvp(compile_dash[0], compile_dash);
        fprintf(stderr, "%s: command not found\n", compile_dash[0]);
        _exit(-1);
    }
    int status;
    wait(&status);
    if (!(WIFEXITED(status) && WEXITSTATUS(status) == 0)) {
        fprintf(stderr, "Failed to compile dash\n");
        exit(-1);
    }
    unlink("/stage1/bin/dash");
    unlink("/bin/dash");
    unlink("/bin/sh");
    unlink("/bin/tcc");
    unlink("/bin/cc");
    res = link("/src/dash/src/dash", "/stage1/bin/dash") ||
        symlink("/stage1/bin/dash", "/bin/dash") ||
        symlink("/stage1/bin/dash", "/bin/sh") ||
        symlink("/stage1/bin/tcc", "/bin/tcc") ||
        symlink("/stage1/bin/tcc", "/bin/cc");
    if (res) {
        fprintf(stderr, "Failed to install dash\n");
        exit(-1);
    }
    color_echo("Compiled dash!");

    // Exec continuation script with our brand new shell
    printf("Jumping in the compiled shell!\n");
    execvp(continue_sh[0], continue_sh);
    fprintf(stderr, "%s: command not found\n", continue_sh[0]);
    _exit(-1);
}
